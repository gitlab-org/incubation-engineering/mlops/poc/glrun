from distutils.core import setup

setup(
    name='citer',
    packages=[],
    version='0.0.1',
    license='MIT',
    description='Run gitlab pipelines without needing to Commit',
    author='Eduardo Bonet',  # Type in your name
    author_email='eduardo.bonet@gitlab.com',  # Type in your E-Mail
    url='https://gitlab.com/gitlab-org/incubation-engineering/mlops/poc/citer',
    download_url='',
    keywords=['CI', 'GITLAB', 'DEVOPS'],  # Keywords that define your package best
    install_requires=[
        'click',
        'requests',
        'urllib3'
    ],
    scripts=['bin/citer'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.9',
    ],
)
